<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

            //----------------Index section----------------//
//index home: display the home page
Route::get('/', 'IndexController@index')->name('home');

            //----------------Shop section----------------//
//shop index: display the shop home page
Route::get('boutique', 'ShopController@index')->name('shop');
//shop show: display the product page
Route::get('boutique/{boutique}', 'ShopController@show')->name('shop.show');

            //----------------Shopping cart section----------------//
//cart index: display the cart home page
Route::get('/panier', 'CartController@show')->name('cart.show');
//cart addToCart: pretty explanatory
Route::post('/panier', 'CartController@addToCart')->name('cart.addToCart')->middleware('auth');
//cart reduceByOne: reduce by one the number of product in our cart
Route::get('/panier/reduce/{id}', 'CartController@reduceByOne')->name('cart.reduceByOne')->middleware('auth');
//cart increaseByOne: increase by one the number of product in our cart
Route::get('/panier/increase/{id}', 'CartController@increaseByOne')->name('cart.increaseByOne')->middleware('auth');
//cart removeItem: remove the item from our cart
Route::get('/panier/remove/{id}', 'CartController@removeItem')->name('cart.removeItem')->middleware('auth');
//cart buyCart: finish the transaction and forget the cart in the session
Route::post('/panier/buy', 'CartController@buyCart')->name('cart.buyCart')->middleware('auth');
//cart complete: complete order
Route::post('panier/{panier}', 'CartController@complete')->name('cart.complete')->middleware('auth', 'isadmin');

            //----------------Activity section----------------//
//activity index: display the activity home page
Route::get('manifestations', 'ActivityController@index')->name('activities');
//activity create: display the activity creation page
Route::get('manifestations/create', 'ActivityController@create')->name('activities.create')->middleware('auth');
//activity store: store elements in our db
Route::post('manifestations', 'ActivityController@store')->name('activities.store')->middleware('auth');
//activity show: display the activity page
Route::get('manifestations/{manifestations}', 'ActivityController@show')->name('activities.show');
//activity edit: display the activity edition page
Route::get('manifestations/{manifestations}/edit', 'ActivityController@edit')->name('activities.edit')->middleware('auth');
//activity update: update elements in our database
Route::put('manifestations/{manifestations}', 'ActivityController@update')->name('activities.update')->middleware('auth');
//activity destroy: delete elements in our database
Route::delete('manifestations/{manifestations}', 'ActivityController@destroy')->name('activities.destroy')->middleware('auth');
//activity joinActivity: also explanatory
Route::post('/manifestations/join/{id}', 'ActivityController@joinActivity')->name('activities.join')->middleware('auth');
//activity leaveActivity: as explanatory as the other one
Route::post('/manifestations/leave/{id}', 'ActivityController@leaveActivity')->name('activities.leave')->middleware('auth')->middleware('auth');
//activity store: store elements in our db
Route::post('/manifestations/{manifestations}/postPhoto', 'ActivityController@postPhoto')->name('activities.postPhoto')->middleware('auth');
//activity showAlbum: display the photo album home page
Route::get('/manifestations/albumphoto/{id}', 'ActivityController@showAlbum')->name('activities.showAlbum');
//activity postPhoto: post a photo and store it
Route::post('/manifestations/{manifestations}/postPhoto', 'ActivityController@postPhoto')->name('activities.postPhoto')->middleware('auth');
//activity postComment: post a comment and store it
Route::post('/manifestations/{manifestations}/postComment', 'ActivityController@postComment')->name('activities.postComment')->middleware('auth');
//activity postLike: post a like and store it
Route::post('/manifestations/{manifestations}/postLike', 'ActivityController@postLike')->name('activities.postLike')->middleware('auth');
//activity postLike: remove a like
Route::post('/manifestations/{manifestations}/removeLike', 'ActivityController@removeLike')->name('activities.removeLike')->middleware('auth');
//activity activate: activate the activity
Route::post('manifestations/{manifestations}', 'ActivityController@activate')->name('activities.activate')->middleware('auth', 'isadmin');


//----------------Login section----------------//
//login showLoginForm: display the login home page
Route::get('connexion', 'Auth\LoginController@showLoginForm')->name('login');
//login login: log the user in
Route::post('connexion', 'Auth\LoginController@login')->name('login.post');
//login logout: log the user out
Route::get('deconnexion', 'Auth\LoginController@logout')->name('logout');

//----------------Register section----------------//
//register showRegistrationForm: display the registration home page
Route::get('inscription', 'Auth\RegisterController@showRegistrationForm')->name('register');
//register register: register the user
Route::post('inscription', 'Auth\RegisterController@register')->name('register.post');

//----------------User section----------------//
//user myInfos: display the user home page
Route::get('/mon-compte/mes-informations', 'UserController@myInfos')->name('account.infos')->middleware('auth');
//user updateMyInfos: update elements in our database
Route::post('/mon-compte/mes-informations', 'UserController@updateMyInfos')->name('account.infos.post')->middleware('auth');
//my activity
Route::get('mon-compte/mes-activites', 'UserController@myActivities')->name('activities.account')->middleware('auth');
////user myInfos: display the user orders home page
Route::get('/mon-compte/mes-commandes', 'UserController@myOrders')->name('account.orders')->middleware('auth');

//----------------Password section----------------//
//password index: display the activity home page
Route::get('change_mdp', 'ChangePswdController@index')->name('password-reset')->middleware('auth');
//password update: update elements in our database
Route::post('change_mdp/{change_mdp}', 'ChangePswdController@update')->name('password-reset.post')->middleware('auth');

//-----------------Admin section-------------------//
Route::get('admin', 'Admin\AdminPanelController@index')->name('admin.index')->middleware('auth', 'isadmin');
Route::get('admin/achats', 'Admin\AdminPanelController@Getpurchases')->name('admin.purchase')->middleware('auth', 'isadmin');

Route::get('admin/manifestations', 'Admin\AdminPanelController@adminActivities')->name('admin.activity')->middleware('auth', 'isadmin');
Route::get('admin/manifestations/download/{id}', 'Admin\AdminPanelController@downloadUsers')->name('admin.activities.downloadUsers')->middleware('auth', 'isadmin');


Route::get('admin/produits', 'Admin\AdminPanelController@adminStore')->name('admin.store')->middleware('auth', 'isadmin');
//shop create: display the product creation page
Route::get('admin/produits/create', 'ShopController@create')->name('shop.create')->middleware('auth', 'isadmin');
//shop store: store elements in our db
Route::post('admin/produits', 'ShopController@store')->name('shop.store')->middleware('auth', 'isadmin');
//shop edit: display the product edition page
Route::get('admin/produits/{boutique}/edit', 'ShopController@edit')->name('shop.edit')->middleware('auth', 'isadmin');
//shop update: update elements in our database
Route::put('admin/produits/{boutique}', 'ShopController@update')->name('shop.update')->middleware('auth', 'isadmin');
//shop destroy: delete elements in our database
Route::delete('admin/produits/{boutique}', 'ShopController@destroy')->name('shop.destroy')->middleware('auth', 'isadmin');
Route::get('admin/utilisateurs', 'Admin\AdminUsers@getUsers')->name('admin.users')->middleware('auth', 'isadmin');
Route::get('admin/utilisateurs/{user}', 'Admin\AdminUsers@getUser')->name('admin.user')->middleware('auth', 'isadmin');
Route::post('admin/utilisateurs/{user}', 'Admin\AdminUsers@editUser')->name('admin.user.edit')->middleware('auth', 'isadmin');


//----------------Category section----------------//
//category add: add a category
Route::post('category', 'CategoryController@add')->name('category.add')->middleware('auth', 'isadmin');

### To setup a Laravel Project:

    composer install
    npm install
    copy .env.example .env (Windows)
    cp .env.example .env (Linux/Mac)
    php artisan key:generate

### Run development server:
    
    php artisan serve
    
### Clear cache:
    
    php artisan cache:clear 
    php artisan config:clear

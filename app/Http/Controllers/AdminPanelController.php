<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\PurchaseProduct;
use Illuminate\Http\Request;
use App\Models\Purchase;

class AdminPanelController extends Controller
{

    public function index(Request $request)
    {
        return view('admin.index_admin');
    }

    public
    function getPurchases()
    {
        $purchases = Purchase::all();

        return view('admin\admin.purchase')
            ->with(['purchases' => $purchases]);
    }

}

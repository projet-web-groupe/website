<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/mon-compte';

    protected $apiKey = '';
    protected $loginEndpoint = 'http://localhost:3000/auth/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->apiKey = env('API_KEY');
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        $body = array('email' => $request->input('email'), 'password' => $request->input('password'));
        $headers = ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->apiKey];

        $client = new \GuzzleHttp\Client();

        $message = 'Impossible de contacter l\'API National';

        try {
            $res = $client->post($this->loginEndpoint, ['body' => json_encode($body), 'headers' => $headers]);

            $user = json_decode($res->getBody()->getContents())->user;

            $userModel = User::find($user->id);

            if ($userModel === null) {
                $this->create(array('id' => $user->id, 'email' => $user->email));
                $userModel = User::find($user->id);
            }

            Auth::login($userModel);
            return redirect('/');
        } catch (RequestException $requestException) {
            if ($requestException->hasResponse()) {
                $message = json_decode($requestException->getResponse()->getBody()->getContents())->error;
            }
            return redirect('/connexion')->with(['error' => $message])->withInput();
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        Log::info($data['id']);
        $user = new User(['id'=> $data['id'], 'email' => $data['email']]);
        $role = Role::find(1);
        $user->role()->associate($role);
        $user->save();
        return $user;
    }
}

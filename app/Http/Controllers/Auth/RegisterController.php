<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/inscription';

    protected $apiKey = '';
    protected $registerEndpoint = 'http://localhost:3000/auth/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->apiKey = env('API_KEY');
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'location' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed',
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',    // must contain at least one digit
                ],
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $body = array(
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'location' => $request->input('location')
        );

        $headers = ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->apiKey];

        $client = new \GuzzleHttp\Client();

        $message = 'Impossible de contacter l\'API National';

        try {
            $res = $client->post($this->registerEndpoint, ['body' => json_encode($body), 'headers' => $headers]);

            $user = json_decode($res->getBody()->getContents())->user;

            event(new Registered($user = $this->create($request->all() + ['id' => $user->id])));

            return $this->registered($request, $user)
                ?: redirect($this->redirectPath())->with(['success' => 'Veuillez confirmer votre e-mail']);

        } catch (RequestException $requestException) {
            if ($requestException->hasResponse()) {
                $message = json_decode($requestException->getResponse()->getBody()->getContents())->error;
            }
            return redirect('/inscription')->with(['error' => $message])->withInput();
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        Log::info($data['id']);
        $user = new User(['id'=> $data['id'], 'email' => $data['email']]);
        $role = Role::find(1);
        $user->role()->associate($role);
        $user->save();
        return $user;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Models\Purchase;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{

    /**
     * Store in session.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addToCart(Request $request)
    {
        $id = $request->input('id');
        $product = Product::find($id);
        $oldCart = $request->session()->has('cart') ? $request->session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);

        return back()->with('success', 'Objet ajouté au panier');
    }

    public function reduceByOne(Request $request, $id)
    {
        $oldCart = $request->session()->has('cart') ? $request->session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);

        if (count($cart->items) > 0) {
            $request->session()->put('cart', $cart);
        } else {
            $request->session()->forget('cart');
        }

        return back()->with('success', 'Objet supprimé du panier');
    }

    public function increaseByOne(Request $request, $id)
    {
        $oldCart = $request->session()->has('cart') ? $request->session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->increaseByOne($id);
        $request->session()->put('cart', $cart);
        return back()->with('success', 'Objet ajouté au panier');
    }

    public function removeItem(Request $request, $id)
    {
        $oldCart = $request->session()->has('cart') ? $request->session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        if (count($cart->items) > 0) {
            $request->session()->put('cart', $cart);
        } else {
            $request->session()->forget('cart');
        }

        return back()->with('success', 'Objet supprimé du panier');
    }

    public function show(Request $request)
    {
        if (!$request->session()->has('cart')) {
            return view('cart.cart_index');
        }
        $oldCart = $request->session()->get('cart');
        $cart = new Cart($oldCart);
        return view('cart.cart_index', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }

    public function buyCart(Request $request)
    {

        $oldCart = $request->session()->get('cart');
        $cart = new Cart($oldCart);

        $purchase = new Purchase();
        $purchase->totalPrice = $cart->totalPrice;
        $purchase->user()->associate(Auth::user());
        $purchase->save();

        foreach ($cart->items as $item) {
            $purchase->products()->attach($item['item']['id'], ['quantity' => $item['qty'], 'price' => $item['item']['price']]);
        }

        Mail::raw('Merci d\'avoir commandé un/des article(s) sur la boutique du BDE ! Nous revenons vers vous au plus vite pour vous remettre votre commande.', function ($message) {
            $message->subject('Vous venez d\'effectuer un achat !');
            $message->from('no-reply@barbec.cool', env('APP_NAME'));
            $message->to(Auth::user()->email);
        });

        $user = app('App\Http\Controllers\UserController')->fetchUser(Auth::user()->id);

        Mail::raw($user->firstname . ' ' . $user->lastname . ' a commandé sur la boutique. Veuillez consulter le panel admin', function ($message) {
            $message->subject('Nouvel achat sur la boutique');
            $message->from('no-reply@barbec.cool', env('APP_NAME'));
            $message->to('a.l.o.u.a.r.r.a.n.i@gmail.com');
        });

        $request->session()->forget('cart');
        return redirect('/boutique')->with('success', 'Achat effectué, un mail va vous etre envoyé');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function complete(Request $request, $id){
        $purchase = Purchase::find($id);
        $purchase->update(['status' => 'complete']);
        return back();
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Models\Product;
use App\Models\Purchase;
use Illuminate\Http\Request;
use League\Csv\Writer;

class AdminPanelController extends Controller
{

    public function index(Request $request)
    {
        return view('admin.index_admin');
    }

    public function getPurchases()
    {
        $purchases = Purchase::all();

        return view('admin.purchase_admin')
            ->with(['purchases' => $purchases]);
    }

    public function adminActivities()
    {
        $activities = Activity::all();

        return view('admin.activity_admin')
            ->with(['activities' => $activities]);
    }

    public function downloadUsers(Request $request, $id)
    {
        $activity = Activity::find($id);
        $allUsers = Activity::find($id)->users()->get();

        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        $csv->setDelimiter(';');

        $csv->insertOne(['Manifestation :', $activity->title]);
        $csv->insertOne(['Date :', $activity->rendezvous]);
        $csv->insertOne(['']);
        $csv->insertOne(['']);
        $csv->insertOne(['Participants :']);

        $csv->insertOne(['Nom', 'Prenom', 'Adresse E-mail']);

        foreach ($allUsers as $user) {
            $user = app('App\Http\Controllers\UserController')->fetchuser($user->id);
            $csv->insertOne([$user->lastname, $user->firstname, $user->email]);
        }

        if (count($allUsers) < 1) {
            $csv->insertOne(['Pas de participants :(']);
        }

        $csv->output('users.csv');
    }

    public function adminStore()
    {
        $products = Product::all();

        return view('admin.store_admin')
            ->with(['products' => $products]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminUsers extends Controller
{
    public function getUsers()
    {
        $allUsers = User::all();
        $users = [];
        foreach ($allUsers as $user) {
            $users[] = app('App\Http\Controllers\UserController')->fetchUser($user->id);
        }

        return view('admin.admin_users')->with(['users' => $users]);
    }

    public function getUser(Request $request, $id)
    {
        $user = User::find($id);
        $user = app('App\Http\Controllers\UserController')->fetchUser($user->id);

        return view('admin.admin_user')->with(['user' => $user]);
    }

    public function editUser(Request $request, $id)
    {
        $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'location' => 'required|string',
        ]);

        $data = array(
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'location' => $request->input('location')
        );
        if (app('App\Http\Controllers\UserController')->updateUser(Auth::user()->id, $data)) {
            return back()->with('success', 'Mis à jour !');
        }
        return back()->with('error', 'Impossible de mettre à jour l\'utilisateur');

    }
}

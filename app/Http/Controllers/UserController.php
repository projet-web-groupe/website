<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Purchase;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $apiKey = '';
    protected $endpointUser = 'http://localhost:3000/users/:id';

    public function __construct()
    {
        $this->apiKey = env('API_KEY');
    }

    public function fetchUser(string $userId)
    {
        $headers = ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->apiKey];
        $client = new \GuzzleHttp\Client();

        try {
            $res = $client->get(str_replace(':id', $userId, $this->endpointUser), ['headers' => $headers]);
            return json_decode($res->getBody()->getContents())->user;
        } catch (RequestException $requestException) {
            return NULL;
        }
    }

    public function updateUser(string $userId, array $data)
    {
        $headers = ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . $this->apiKey];
        $client = new \GuzzleHttp\Client();

        try {
            $client->put(str_replace(':id', $userId, $this->endpointUser), ['json' => $data, 'headers' => $headers]);
            return true;
        } catch (RequestException $requestException) {
            return false;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function myInfos(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();
        $user = $this->fetchUser($userId);
        $error = NULL;
        if ($user === NULL)
            $error = 'Impossible de contacter l\'API National';

        return view('account.infos')->with(['user' => $user, 'error' => $error]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateMyInfos(Request $request)
    {
        $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'location' => 'required|string',
        ]);

        $data = array(
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'location' => $request->input('location')
        );
        if ($this->updateUser(Auth::user()->getAuthIdentifier(), $data)) {
            return redirect('/mon-compte/mes-informations')->with('success', 'Vos informations ont bien été mis à jour !');
        }
        return redirect('/mon-compte/mes-informations')->with('error', 'Impossible de mettre à jour vos informations.');
    }

    public function myActivities(Request $request)
    {
        $myActivities = Auth::user()->activities()->get();
        return view('account.my-activities')->with([
        'activities' => $myActivities
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function myOrders(Request $request)
    {
        $purchases = Auth::user()->purchases;

        return view('account.orders')->with([
            'purchases' => $purchases
            ]);
    }


}

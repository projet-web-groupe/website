<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\PurchaseProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = ProductCategory::where('status', 'active')->get();

        $filter = $request->input('category');
        $sortMethod = $request->input('sortMethod');
        $sortField = $request->input('sortField');

        if ($filter && $sortMethod && $sortField) {
            $products = Product::where(['category_id' => $filter, 'status' => 'active'])->orderBy($sortField, $sortMethod)->get();
        } elseif (!$filter && $sortMethod && $sortField) {
            $products = Product::where('status', 'active')->orderBy($sortField, $sortMethod)->get();
        } else {
            $products = Product::where('status', 'active')->orderBy('updated_at', 'desc')->get();
        }

        $bestPurchase = PurchaseProduct::select('url')->join('product', 'purchase__product.product_id', '=', 'product.id')
            ->join('product__photo', 'purchase__product.product_id', '=', 'product__photo.product_id')
            ->join('photo', 'product__photo.product_id', '=', 'photo.id')
            ->orderBy('quantity', 'DESC')
            ->limit('3')->get();

        return view('shop\shop_index', compact('bestPurchase', 'categories', 'products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        $categories = ProductCategory::where('status', 'active')->get();
        return view('shop\shop_create', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'description' => 'required',
            'category' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        //Create post
        $category = ProductCategory::find($request->input('category'));

        $product = new Product();
        $photo = new Photo();

        $destinationPath = $request->file('file')->store('/public/galeryImages/productImages');
        $photo->url = str_replace('public', 'storage', "/" . $destinationPath);
        $photo->save();

        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->stock = $request->input('stock');
        $product->category()->associate($category);
        $product->save();
        $product->photos()->attach($photo->id, ['order' => 1]);

        return redirect('/boutique')->with('success', 'Vente crée');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        $product = Product::find($id);
        return view('shop\shop_show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        $product = Product::find($id);
        $categories = ProductCategory::where('status', "active")->get();
        return view('shop\shop_edit', [
            'categories' => $categories
        ])->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'description' => 'required',
            'category' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        //Create post
        $category = ProductCategory::find($request->input('category'));

        $product = Product::find($id);
        $photos = $product->photos()->get();
        $photo = $photos[0];

        $destinationPath = $request->file('file')->store('/public/galeryImages/productImages');
        $photo->url = str_replace('public', 'storage', "/" . $destinationPath);
        $photo->save();

        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->stock = $request->input('stock');
        $product->category()->associate($category);
        $product->save();

        return redirect('/boutique')->with('success', 'Vente updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $product = Product::find($id);
        $photos = $product->photos()->get();
        foreach ($photos as $photo) {
            $photo->delete();
        }
        $product->delete();
        return back()->with('success', 'Vente supprimée');
    }
}

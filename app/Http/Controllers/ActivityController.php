<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\ActivityComment;
use App\Models\ActivityLike;
use App\Models\ActivityRegister;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = Activity::where('status', 'active')->orderBy('updated_at', 'desc')->get();
        return view('activity\activity_index', [
        ])->with('activities', $activities);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('activity\activity_create', [
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'rendezvous' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        //Create post

        $activity = new Activity();
        $photo = new Photo();

        $destinationPath = $request->file('file')->store('/public/galeryImages/acivityImages');
        $photo->url = str_replace('public', 'storage', "/" . $destinationPath);
        $photo->save();

        $activity->title = $request->input('title');
        $activity->description = $request->input('description');
        $activity->rendezvous = $request->input('rendezvous');
        $activity->user()->associate(Auth::user());
        $activity->save();

        $activity->photos()->attach($photo->id, ['order' => 1]);

        return redirect('/manifestations')->with('success', 'Activité crée');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = ActivityComment::where('status', 'active')->get();
        foreach ($comments as $comment) {
            $user = app('App\Http\Controllers\UserController')->fetchuser($comment->user->id);
            $comment->user->firstname = $user->firstname;
            $comment->user->lastname = $user->lastname;
        }

        $usersId = [];
        $users = ActivityRegister::all();
        foreach ($users as $user) {
            $usersId[] = $user->user_id;
        }

        $activity = Activity::find($id);

        $likes = ActivityLike::all();
        $likesId = [];
        foreach ($likes as $like) {
            $likesId[] = $like->user_id;
        }

        return view('activity\activity_show', [
            'comments' => $comments,
            'usersId' => $usersId,
            'likesId' => $likesId
        ])->with('activity', $activity);
    }

    /**
     * Post a comment.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function postComment(Request $request, $id)
    {

        $request->validate([
            'comment' => 'required',
        ]);

        $activity = Activity::find($id);

        $comment = new ActivityComment;
        $comment->comment = $request->input('comment');
        $comment->activity()->associate($activity);
        $comment->user()->associate(Auth::user());

        $comment->save();

        return back()->with('success', 'Merci votre commentaire est en attente d\'approbation !');
    }

    /**
     * Post a photo.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function postPhoto(Request $request, $id)
    {
        $request->validate([
            'file' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        $activity = Activity::find($id);
        $photo = new Photo();
        $destinationPath = $request->file('file')->store('/public/galeryImages/acivityImages');
        $photo->url = str_replace('public', 'storage', "/" . $destinationPath);
        $photo->save();

        $activity->userphotos()->attach($photo->id, ['user_id' => Auth::user()->id]);

        return back()->with('success', 'Photo posté');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showAlbum($id)
    {
        $activity = Activity::find($id);
        $photos = $activity->userphotos()->get();

        return view('activity\userphoto_show')->with([
            'activity' => $activity,
            'photos' => $photos,
        ]);
    }

    /**
     * Join an activity.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function joinActivity($id)
    {
        $activity = Activity::find($id);
        $activity->users()->attach(Auth::user()->id);
        $activity->save();

        return back()->with('success', "Vous etes bien inscrit à l'activité");
    }

    /**
     * Leave an activity.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function leaveActivity($id)
    {
        $activity = Activity::find($id);

        $activity->users()->detach(Auth::user()->id);
        $activity->save();

        return back()->with('success', "Vous etes bien de-inscrit de l'activité");
    }


    /**
     * Post a like.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function postLike(Request $request, $id)
    {
        $activity = Activity::find($id);

        $like = new ActivityLike;
        $like->activity()->associate($activity);
        $like->user()->associate(Auth::user());

        $like->save();

        return back()->with('success', 'Manifestation liké !');
    }

    /**
     * Remove a like.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function removeLike(Request $request, $id)
    {
        $activity = Activity::find($id);
        $activity->likes()->detach(Auth::user()->id);

        return back()->with('success', 'Commentaire posté');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity = Activity::find($id);
        return view('activity\activity_edit', [])->with('activity', $activity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'rendezvous' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        //Create post

        $activity = Activity::find($id);
        $photos = $activity->photos()->get();
        $photo = $photos[0];

        $destinationPath = $request->file('file')->store('/public/galeryImages/acivityImages');
        $photo->url = str_replace('public', 'storage', "/" . $destinationPath);
        $photo->save();

        $activity->title = $request->input('title');
        $activity->description = $request->input('description');
        $activity->rendezvous = $request->input('rendezvous');
        $activity->save();

        return redirect('/manifestations')->with('success', 'Activite updated');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id){
        $activity = Activity::find($id);
        $activity->update(['status' => 'active']);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::find($id);
        $activity->delete();
        return back()->with('success', 'Activité supprimée');
    }



}

<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * Add data in our database.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {

        $request->validate([
            'category' => 'required',
        ]);

        $category = new ProductCategory();
        $category->name = $request->input('category');
        $category->save();
        return back();
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Table name
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'price', 'reduction', 'status'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory');
    }

    public function purchase_product()
    {
        return $this->HasMany('App\Models\PurchaseProduct');
    }

    public function photos()
    {
        return $this->belongsToMany('App\Models\Photo', 'product__photo');
    }
}

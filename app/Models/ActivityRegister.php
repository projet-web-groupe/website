<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityRegister extends Model
{
    //Table name
    protected $table = 'activity__register';

    public $timestamps = true;
}

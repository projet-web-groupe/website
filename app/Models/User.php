<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    //Table name
    protected $table = 'user';

    protected $firstname;
    protected $lastname;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'lastname', 'firstname', 'email', 'password', 'location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return Auth::user()->role->name == 'admin';
    }

    public function isCesiMember()
    {
        return Auth::user()->role->name == 'cesi_member';
    }

    public function isStudent()
    {
        return Auth::user()->role->name == 'student';
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function activity_comment()
    {
        return $this->HasMany('App\Models\ActivityComment');
    }

    public function activities()
    {
        return $this->belongsToMany('App\Models\Activity', 'activity__register');
    }

    public function activity_register()
    {
        return $this->HasMany('App\Models\ActivityRegister');
    }

    public function activity_user_photo()
    {
        return $this->HasMany('App\Models\ActivityUserPhoto');
    }

    public function purchases(){
        return $this->HasMany('App\Models\Purchase');
    }
}

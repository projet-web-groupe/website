<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //Table name
    protected $table = 'activity';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'descriptions', 'status', 'photo_url'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function photos()
    {
        return $this->belongsToMany('App\Models\Photo', 'activity__photo');
    }

    public function userphotos()
    {
        return $this->belongsToMany('App\Models\Photo', 'activity__user__photo');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'activity__register');
    }

    public function likes(){
        return $this->belongsToMany('App\Models\User', 'activity__likes');
    }
}

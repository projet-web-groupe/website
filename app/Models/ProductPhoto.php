<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPhoto extends Model
{
    //Table name
    protected $table = 'product__photo';

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function photo()
    {
        return $this->belongsTo('App\Models\Photo');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    //Table name
    protected $table = 'product__category';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status'
    ];

    public function purchase_category(){
        return $this->HasOne('App\Models\Purchase_Category');
    }
}

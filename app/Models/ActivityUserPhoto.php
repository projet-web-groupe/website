<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityUserPhoto extends Model
{
    //Table name
    protected $table = 'activity__user__photo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status'
    ];

    public function photo()
    {
        return $this->belongsToMany('App\Models\Photo');
    }

    public function activity()
    {
        return $this->belongsTo('App\Models\Activity');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}

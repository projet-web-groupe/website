<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityPhoto extends Model
{
    //Table name
    protected $table = 'activity__photo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url'
    ];

    public function activity()
    {
        return $this->belongsToMany('App\Models\Activity');
    }

    public function photo()
    {
        return $this->belongsToMany('App\Models\Photo');
    }
}

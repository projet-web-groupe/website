<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseProduct extends Model
{
    //Table name
    protected $table = 'purchase__product';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quantity'
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function purchases()
    {
        return $this->belongsToMany('App\Models\Purchases');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLike extends Model
{
    //Table name
    protected $table = 'activity__likes';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function activity()
    {
        return $this->belongsTo('App\Models\Activity');
    }
}

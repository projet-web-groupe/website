<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
        });

        DB::table('role')->insert(
            array(
                'id' => 1,
                'name' => 'student'
            )
        );
        DB::table('role')->insert(
            array(
                'id' => 2,
                'name' => 'cesi_member'
            )
        );
        DB::table('role')->insert(
            array(
                'id' => 3,
                'name' => 'admin'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role');
    }
}

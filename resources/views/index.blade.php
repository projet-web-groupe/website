@extends('layouts.app')
@section('title', 'Accueil')
@section('content')


  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">
      <li data-target="#carouselExampleIndicators" data-slide-to="1">
      <li data-target="#carouselExampleIndicators" data-slide-to="2">
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="http://dev.meilleures-licences.com/photo_ecole/photoeicesi-1448636468.jpg" class="d-block w-100"
             alt="..."/>
      </div>
      <div class="carousel-item">
        <img src="https://lyon.cesi.fr/wp-content/uploads/sites/30/2018/11/CESI_Campus_JPO-1312x711-2.png"
             class="d-block w-100" alt="..."/>
      </div>
      <div class="carousel-item">
        <img src="https://www.ecully.fr/fileadmin/_processed_/e/5/csm_Campus-CESI_7f307fe881.jpg"
             class="d-block w-100" alt="..."/>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container-fluid " id="homepage">

    <h2><br/>Manifestations à venir : </h2>
    <div class="container">
      @if(count($activities) > 0)
        <div class="row ">
          @foreach($activities as $activity)
            <div class="col-lg-4 col-sm my-4 space">
              <div class="card">
                @if(count(($activity->photos)) > 0)
                  <img src="{{ $activity->photos[0]->url }}" class="img-fluid" alt="image de la manifestation">
                @else
                  <img src="" alt="" class="img-fluid">
                @endif
                <div class="card-body">
                  <div class="card-header">
                    {{$activity->title}}
                  </div>
                  <p class="card-title"><strong>{{$activity->firstname}}</strong></p>
                  <p class="card-text">{!! $activity->description !!}</p>

                  <a href="/manifestations/{{$activity->id}}"
                     class="col text-center space btn btn-primary">Détails</a>
                </div>
              </div>
            </div>
          @endforeach
        </div>

      @else
        <p class="text-center"><strong>Pas de d'activité !</strong></p>
      @endif
    </div>
    <div><br/>
      <h2>Découvrir le CESI lyon :</h2>
      <div>
        <a href="https://lyon.cesi.fr/" target="_blank">
          <img src="https://www.cesi.fr/wp-content/uploads/2018/11/logo-CESI.png" class="d-block w-100"
               alt="Logo du cesi"/></a>
      </div>
    </div>
  </div>
@endsection


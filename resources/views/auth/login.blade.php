@extends('layouts.app')
@section('title', 'Connexion')
@section('content')
  <div class="container my-4">

    <div class="row justify-content-center">
      <div class="col-12 col-md-6 text-center">
        <h1>Connexion</h1>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-12 col-md-6">

        @include('layouts\message')

        <form action="{{route('login')}}" method="post">
          @csrf
          <div class="form-row">
            <div class="form-group col-12">
              <label for="email" htmlFor="formGroupExampleInput">Email</label>
              <label for="formGroupExampleInput"></label>
              <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="formGroupExampleInput" value="{{old('email')}}" required autocomplete="email" autofocus placeholder="Email" />
              @error('email')
              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
              @enderror
            </div>

            <div class="form-group col-12">
              <label for="password" htmlFor="formGroupExampleInput">Mot de passe</label>
              <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                     id="password" placeholder="******" required autocomplete="current-password" />
              @error('password')
              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
              @enderror
            </div>
            <a href="" class="extra-login-register"><b>Mot de passe oublié ?</b></a>
          </div>

          <div class="col-12 text-center my-3">
            <button type='submit' class="btn btn-primary btn-lg">{{ __('Login') }}</button>
            @if (Route::has('password.request'))
              <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
              </a>
            @endif
          </div>
        </form>
        <div>
          <b>Vous n'avez pas encore d'espace personnel ? <br />
            <a href="{{Route('register')}}" class="extra-login-register">Inscrivez-vous !</a></b>
        </div>
      </div>
    </div>
  </div>
@endsection

@extends('layouts.app')
@section('title', 'Inscription')
@section('content')

  <div class="container my-4">

    <div class="row justify-content-center">
      <div class="col-12 col-md-6 text-center">
        <h1>Inscription</h1>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-12 col-md-6">

        @include('layouts\message')

        <form action="" method="post">
          @csrf
          <div class="form-row">
            <div class="form-group col-12">
              <label htmlFor="formGroupExampleInput">Email</label>
              <label for="email"></label>
              <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Email" value="{{old('email')}}" required autocomplete="email" autofocus/>
              @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-12">
              <label htmlFor="formGroupExampleInput">Prénom</label>
              <label for="formGroupExampleInput"></label>
              <input type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" id="formGroupExampleInput" placeholder="Prénom" value="{{old('firstname')}}" required autocomplete="name" autofocus/>
              @error('firstname')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>

            <div class="form-group col-12">
              <label htmlFor="formGroupExampleInput">Nom</label>
              <label for="lastname">

              </label>
              <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror"
                             name="lastname" placeholder="Nom" value="{{ old('lastname') }}" required
                             autocomplete="lastname" autofocus>
              @error('lastname')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>

            <div class="form-group col-12">
              <label htmlFor="formGroupExampleInput">Location</label>
              <label for="location"></label>
              <input id="location" type="text" class="form-control @error('location') is-invalid @enderror"
                     name="location" placeholder="Location" value="{{ old('location') }}" required autofocus>

              @error('location')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>

          </div>

          <div class="form-row">
            <div class="form-group col-12 col-sm-6">
              <label for="password">Mot de passe</label>
              <input id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" type="password" class="form-control @error('password') is-invalid @enderror"
                     placeholder="Mot de passe" name="password" required autocomplete="new-password">

              @error('password')
              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
              @enderror
            </div>

            <div class="form-group col-12 col-sm-6">
              <label for="password-confirm">Confirmation du mot de passe</label>
              <input id="password-confirm" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" type="password"
                     class="form-control @error('password') is-invalid @enderror"
                     placeholder="Confirmation mot de passe" name="password_confirmation" required
                     autocomplete="new-password">

              @error('password')
              <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label for="condition" class="col-md-4 col-form-label text-md-right"></label>
            <div class="col-md-1">
              <input type="checkbox" class="" id="condition" required>
            </div>
            <div>
              <p>Accepter les conditions</p>
            </div>
          </div>


          <div class="col-12 text-center my-3">
            <button type='submit' class="btn btn-primary btn-lg">{{ __('Register') }}</button>
          </div>
        </form>
        <div>
          <b>Vous n'avez pas encore d'espace personnel ? <br/>
            <a href="{{Route('login')}}" class="extra-login-register">
              Connectez-vous !</a></b>
        </div>
      </div>
    </div>
  </div>

@endsection

@extends('layouts.app')
@section('title', 'Mes activitées')
@section('content')

  <div class="container my-3" id="account">
    <h2>Mes manifestations :</h2>
    <hr>
    <div class="row">

      @include('layouts\message')
      @if(count($activities) > 0)
        @foreach($activities as $activity)
          <div class="col-sm-3">
            <div class="card">
              @if(count(($activity->photos)) > 0)
                <img src="{{$activity->photos[0]->url}}" class="card-img-top" alt="">
              @else
                <img class="img-fluid" src="" alt="">
              @endif
              <div class="card-body">
                <h5 class="card-title">{{ $activity->title }}</h5>
                <p class="card-text">{{ $activity->description }}</p>
                <a class="btn btn-primary" href="{{ route('activities.show', $activity->id) }}">Voir</a>
              </div>
            </div>
          </div>
        @endforeach
      @else
        <p class="text-center"><strong>Vous n'avez pas encore participé à une manifestation !</strong></p>
      @endif
    </div>
  </div>

@endsection


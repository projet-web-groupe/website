@extends('layouts.app')

@section('content')
  <div class="container-fluid my-4">
    <div class="row">
      <div class="col-md-8 offset-2">

        <h2>Votre historique de commande</h2>
        <hr>

        @include('layouts\message')

        @foreach($purchases as $purchase)
          <div class="card">
            <div class="card-header">Commande passée le {{$purchase->created_at}}<span
                class="badge alert">{{$purchase->status}}</span></div>
            <div class="card-body">
              <blockquote class="blockquote mb-0">
                @foreach($purchase->purchase_products as $item)
                  <ul class="list-group">
                    <li class="list-group-item">
                      <span class="badge">{{ $item->price }} €</span>
                      | {{$item->product->title}} | {{$item->quantity}} qté
                    </li>
                  </ul>
                @endforeach
                <footer class="blockquote-footer" style="background-color: white">Prix Total H.T.<cite
                    title="Source Title"><strong> € {{ $purchase->totalPrice }}</strong></cite></footer>
              </blockquote>
            </div>
          </div>
          <br>
        @endforeach
      </div>
    </div>
  </div>

@endsection

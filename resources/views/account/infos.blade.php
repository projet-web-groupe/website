@extends('layouts.app') <!-- Extend app.blade.php-->
@section('title', 'Mes informations')
@section('content')
  <div class="container my-4" id="account">

    <div class="row justify-content-center">
      <div class="col-12 col-md-6">
        <h1>Mon Compte</h1>

        <form method="POST" action="{{ route('account.infos.post') }}" enctype="multipart/form-data">

          @include('layouts\message')
          @if($user)
            <div class="form-row my-3"> <!-- Row to align Lastname and Firstname -->

            @csrf

            <!-- Lastname -->
              <div class="form-group col-sm-6">
                <label for="lastname">Nom</label>
                <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $user->lastname }}"
                       placeholder="" />
              </div>

              <!-- Firstname -->
              <div class="form-group col-sm-6">
                <label for="firstname">Prénom</label>
                <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $user->firstname }}"
                       placeholder="" />
              </div>
            </div>
            <!-- Email -->
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" placeholder=""
                     disabled />
            </div>

            <!-- Location-->
            <div class="form-group">
              <label for="location">Localisation</label>
              <input type="text" class="form-control" id="location" name="location" value="{{ $user->location }}"
                     placeholder="" />
            </div>

            <div class="col-12 text-center">
              <button type="submit" class="btn btn-primary">Modifier</button>
            </div>
          @endif
        </form>

      </div>
    </div>

  </div>
@endsection <!-- End Section -->


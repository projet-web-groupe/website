@extends('layouts.app')
@section('title', 'Boutique')
@section('content')

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">
      @foreach($bestPurchase as $best)
        <li data-target="#carouselExampleIndicators" data-slide-to="{{$best->id}}">
      @endforeach
    </ol>
    <div class="carousel-inner" style="max-height: 40vh">

      @php
        $i = 0
      @endphp
      @foreach($bestPurchase as $best)
        @if($i === 0)
          <div class="carousel-item active">
            <img class="d-block w-100" src="{{$best->url}}" alt=""/>
          </div>
        @else
          <div class="carousel-item">
            <img class="d-block w-100" src="{{$best->url}}" alt=""/>
          </div>
        @endif

        {{$i++}}
      @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
       data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
       data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <div class="container-fluid my-4" id="store">
    @include('layouts\message')

    <form method="GET" action="{{route('shop')}}">

      <h3 class="my-4">Filtres</h3>
      <label>
        <select class="custom-select" name="category">
          <option selected="" value="">Open this select menu</option>
          @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
          @endforeach
        </select>
      </label>
      <h3 class="my-4">Tris</h3>
      <label>
        <select class="custom-select" name="sortField">
          <option selected="" value="updated_at">Open this select menu</option>
          <option value="title">Nom</option>
          <option value="price">Prix</option>
          <option value="updated_at">Date</option>
        </select>


        <select class="custom-select my-2" name="sortMethod">
          <option selected="" value="desc">Open this select menu</option>
          <option value="asc">Ascendant</option>
          <option value="desc">Descendant</option>
        </select>
      </label>
      <button type="submit" class="btn btn-primary my-3">Trier</button>
    </form>

    @if(count($products) > 0)
      <div class="row">
        @foreach($products as $product)
          <div class="col-sm-3">
            <div class="card">
              @if(count(($product->photos)) > 0)
                <img src="{{$product->photos[0]->url}}" class="card-img-top" alt="">
              @else
                <img class="card-img-top" src="" alt="">
              @endif
              <div class="card-body">
                <h5 class="card-title">{{$product->title}}</h5>
                <p class="card-text">{{$product->description}}</p>
                <a class="btn btn-primary"
                   href="{{route('shop.show', array('boutique'=>$product->id))}}">Acheter pour {{ $product->price }}
                  €</a>
              </div>
            </div>
          </div>
        @endforeach
      </div>

    @else
      <p class="text-center"><strong>Pas de produits en vente !</strong></p>
    @endif
  </div>

@endsection

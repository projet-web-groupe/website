@extends('layouts.app')<!-- extends app.blade.php-->
@section('title', $product->title)
@section('content') <!--send htlm code into app.balde.php-->
<div class="container-fluid my-4" id="single-activity">

@include('layouts\message') <!--send error message in some case-->

  <!-- bootstarp grid management -->
  <div class="row my-2 justify-content-center">
    <div class="col-sm ">
      <!-- check if the product have a photo of himself  -->
      @if(count(($product->photos)) > 0)
        <img src="{{$product->photos[0]->url}}" class="card-img-top img-responsive img-activity" alt="">
      @else
        <img class="card-img-top img-responsive img-activity" src="" alt="">
      @endif
    </div>
    <div class="col-md-6 col-12 text-center account-summary">
      <!-- infomation on the product -->
      <a href="#" class="block-account"><strong>>{{$product->title}}</strong></a><br /><br />
      <div class="text-left"><strong>Description :</strong></div>
      <br />
      <div>
        <p>{!!$product->description!!}</p>
      </div>
      <div class="text-left"><strong>Information sur le produit :</strong></div>
      <br />
      <div>
        Catégorie du produit <br />Prix : {{$product->price}}€
      </div>
      <br />
      <br />
      <!-- bootstrap grid management -->
      <div class="container">
        <div class="row">
          <div class="col-sm">
            <!-- form to add product in the basket -->
            <form class="" method="POST" action="{{route('cart.addToCart')}}">
              <!-- token protection to validate request -->
              @csrf
              <input name="id" value="{{$product->id}}" type="hidden">
              <button type="submit" class="btn btn-success">Ajouter au panier</button>
            </form>
          </div>
          <!-- button to edit the product -->
          <div class="col-sm">
            <button type="submit" class=" btn btn-danger"><a href="{{ route('shop.edit', $product->id) }}">Editer
                l'article</a></button>
          </div>

          <!-- button to delete an article -->
          <div class="col-sm">
            <form class="" method="POST" action="{{ route('shop.destroy', $product->id) }}">
              <!-- allow form to make delete action -->
              @method('DELETE')
              @csrf

              <button type="submit" class="btn btn-danger">Supprimer</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

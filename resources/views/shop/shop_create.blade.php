@extends('layouts.app')
@section('title', 'Ajout produit')
@section('content')

<div class="container my-4">
@include('layouts\message')
<form method="POST" action="{{route('shop.store')}}" enctype="multipart/form-data" >
    <h1>Créer un produit</h1>
    <div class="form-group">
        @csrf
        <label for="title">Nom du produit</label>
        <input type="text" class="form-control" name="title" placeholder="Nom du produit"/>
    </div>

    <div class="form-group">
        <label for="price">Prix du produit</label>
        <input type="text" class="form-control" name="price" placeholder="Prix du produit"/>
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" cols="30" rows="10"
                  placeholder="Description de l'objet"></textarea>
    </div>

  <div class="form-group">
    <label for="stock">Stock du produit</label>
    <input type="text" class="form-control" name="stock" placeholder="Stock du produit"/>
  </div>

    <div class="form-group">
        <label for="category">Type de produit</label>
        <select class="custom-select" name="category">
            <option selected="">Open this select menu</option>
            @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="file">Select image to upload:</label>
        <input type="file" name="file">
    </div>

    <button type="submit" class="btn btn-primary my-3">Envoyer</button>
</form>

</div>
@endsection

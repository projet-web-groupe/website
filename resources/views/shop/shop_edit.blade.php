@extends('layouts.app')
@section('title', 'Edition produit')
@section('content')
  <div class="container my-4">
    @include('layouts\message')
    <form method="POST" action="{{route('shop.update', $product->id)}}" enctype="multipart/form-data">
      <h1 class="text-center">Editer le produit</h1>
      <div class="form-group">
        @method('PUT')
        @csrf
        <label for="title">Nom du produit :</label>
        <input value="{{ $product->title }}" type="text" class="form-control" name="title"
               placeholder="Nom du produit" />
      </div>

      <div class="form-group">
        <label for="price">Prix du produit :</label>
        <input value="{{ $product->price }}" type="number" class="form-control" name="price"
               placeholder="Prix du produit" />
      </div>

      <div class="form-group">
        <label for="description">Description :</label>
        <textarea class="form-control" name="description" cols="30" rows="10"
                  placeholder="Description de l'objet">{{ $product->description }}</textarea>
      </div>

      <div class="form-group">
        <label for="stock">Stock du produit :</label>
        <input type="number" class="form-control" name="stock" placeholder="Stock du produit"
               value="{{ $product->stock }}" />
      </div>

      <div class="form-group">
        <label for="category">Type de produit :</label>
        <select class="custom-select" name="category">
          <option value="{{ $product->category }}">Changer de catégorie</option>
          @foreach($categories as $category)
            @if($category->id === $product->category_id))
              <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
            @else
              <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endif
          @endforeach
        </select>
      </div>photo

      <div class="form-group">
        <label for="file">Selectionner image qui remplacera:</label>
        <input type="file" name="file">
      </div>

      <button type="submit" class="btn btn-primary">Modifier</button>
    </form>
  </div>
@endsection

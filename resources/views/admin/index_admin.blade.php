@extends('layouts.app')
@section('title', 'Administration')
@section('content')

  <div class="banner">
    <img class="d-block w-100" src="{{ asset('assets/img/banner-admin.jpg') }}" alt="First slide"/>
    <div class="title-centered">Espace admin</div>
  </div>
  <div class="container" id="admin">

    <div class="row justify-content-center">
      <div class="col-12 text-center my-5">
        <a href="{{route('admin.users')}}">
        <button type='submit' class=" m-3 btn btn-secondary btn-lg button-admin">Gérer les
          utilisateurs
        </button>
        </a>
        <a href="{{route('admin.activity')}}">
        <button type='submit' class=" m-3 btn btn-secondary btn-lg button-admin">Gérer les
          manifestations
        </button>
        </a>
        <a href="{{route('admin.purchase')}}">
        <button type='submit'
                class=" m-3 btn btn-secondary btn-lg button-admin">Liste des Achats
        </button>
        </a>
        <a href="{{route('admin.store')}}">
        <button type='submit' class=" m-3 btn btn-secondary btn-lg button-admin">Gérer la
          boutique
        </button>
        </a>
      </div>
    </div>
  </div>
@endsection

@extends('layouts.app')
@section('title', 'Admin commandes')
@section('content')
  <div>
    <div class="banner">
      <img class="d-block w-100" src="{{ asset('assets/img/banner-admin.jpg') }}" alt="First slide"/>
      <div class="title-centered">Espace admin</div>
      <div class="subtitle-centered">Achats</div>
    </div>

    <div class="container-fluid my-3 pl-5" id="archive">
      <p><a href="{{ route('admin.index') }}">Panel Admin</a> / Achats</p>
      <div class="row">
        <table border="1">
          <tbody>
          <tr>
            <th>ID de l'achat</th>
            <th>Status</th>
            <th>ID de l'utilisateur</th>
            <th>Date de l'achat</th>
            <th>Update de l'achat</th>
          </tr>
          @foreach($purchases as $purchase)
            <tr>
              <td>{{ $purchase->id }}</td>
              <td>{{ $purchase->status }}</td>
              <td>{{ $purchase->user_id }}</td>
              <td>{{ $purchase->created_at }}</td>
              <td>{{ $purchase->updated_at }}</td>
              <td>

                @if($purchase->status === "pending")
                  <form method="post" action="{{route('cart.complete', $purchase->id)}}">
                    @csrf
                    <button type="submit" class="btn btn-danger btn-lg management"
                            onclick="return confirm('Voulez vous vraiment clore cette transaction?');">Confirmer et
                      clore la transaction
                    </button>
                  </form>
                @endif

              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

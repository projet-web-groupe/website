@extends('layouts.app')
@section('title', 'Admin manifestations')
@section('content')
  <div>
    <div class="banner">
      <img class="d-block w-100" src="{{ asset('assets/img/banner-admin.jpg') }}" alt="First slide"/>
      <div class="title-centered">Espace admin</div>
      <div class="subtitle-centered">Manifestations</div>
    </div>
    <div class="container-fluid my-3 pl-5" id="archive">
      <p><a href="{{ route('admin.index') }}">Panel Admin</a> / Manifestations</p>
      <div class="row">
        <table border="1">
          <tbody>
          <tr>
            <th>ID</th>
            <th>Titre</th>
            <th>Descritpion</th>
            <th>Status</th>
            <th>Date</th>
            <th>Mise à jour</th>
            <th>ID de l'utilisateur</th>
            <th>Actions</th>
          </tr>
          @foreach($activities as $activity)
            <tr>
              <td class="text-center">{{ $activity->id }}</td>
              <td class="text-center">{{ $activity->title }}</td>
              <td class="text-center">{{ $activity->description }}</td>
              <td class="text-center">{{ $activity->status }}</td>
              <td class="text-center">{{ $activity->created_at}}</td>
              <td class="text-center">{{ $activity->updated_at }}</td>
              <td class="text-center">{{ $activity->user_id }}</td>
              <td>
                <a class="btn btn-primary btn-lg management"
                   href="{{ route('activities.edit', $activity->id) }}">Editer</a>

                @if($activity->status === "pending")
                  <form method="post" action="{{route('activities.activate', $activity->id)}}">
                    @csrf
                    <button type="submit" class="btn btn-success btn-lg management">Accepter l'activité</button>
                  </form>
                @endif

                <form action="{{route('activities.destroy', $activity->id)}}">
                  @method('DELETE')
                  @csrf
                  <button type="submit" class="btn btn-danger btn-lg management">Supprimer</button>
                </form>
                <a href="{{ route('admin.activities.downloadUsers', $activity->id)  }}">Télécharger les inscrits</a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        <div>
          <a class="btn btn-primary btn-sm management float-right" href="{{ route('activities.create') }}">Créer une
            nouvelle activité</a>
        </div>
      </div>
    </div>
  </div>
@endsection

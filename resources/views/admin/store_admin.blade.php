@extends('layouts.app')
@section('title', 'Admin boutique')
@section('content')
  <div>
    <div class="banner">
      <img class="d-block w-100" src="{{ asset('assets/img/banner-admin.jpg') }}" alt="First slide"/>
      <div class="title-centered">Espace admin</div>
      <div class="subtitle-centered">Produits</div>
    </div>
    <div class="container-fluid my-3 pl-5" id="archive">
      <p><a href="{{ route('admin.index') }}">Panel Admin</a> / Boutique</p>
      <div class="row">
        <table border="1">
          <tbody>
          <tr>
            <th>ID</th>
            <th>Titre</th>
            <th>Description</th>
            <th>Prix</th>
            <th>Réduction</th>
            <th>Stock</th>
            <th>Status</th>
            <th>Création</th>
            <th>Mise à jour</th>
            <th>Catégorie</th>
            <th>Actions</th>
          </tr>
          @foreach($products as $product)
            <tr>
              <td class="text-center">{{ $product->id }}</td>
              <td class="text-center">{{ $product->title }}</td>
              <td class="text-center">{{ $product->description }}</td>
              <td class="text-center">{{ $product->price }}€</td>
              @if( $product->reduction === NULL)
                <td class="text-center">0,00€</td>
              @else
                <td class="text-center">{{ $product->reduction }}€</td>
              @endif
              <td class="text-center">{{ $product->stock }}€</td>
              <td class="text-center">{{ $product->status }}</td>
              <td class="text-center">{{ $product->created_at}}</td>
              <td class="text-center">{{ $product->updated_at }}</td>
              <td class="text-center">{{ $product->category->name }}</td>
              <td style="display: flex">
                <a class="btn btn-secondary btn-lg management" href="{{ route('shop.edit', $product->id) }}">Editer le produit</a>
                <form action="{{route('shop.destroy', $product->id)}}">
                  @method('DELETE')
                  @csrf
                  <button type='submit' class="btn btn-danger btn-lg management">Supprimer</button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <form method="post" action="{{route('category.add')}}">
          @csrf
          <button type="submit" class="btn btn btn-sm management">Créer une categorie</button>
          <br>
          <input for="category" name="category" class="text">
        </form>
        <div>
          <a class="btn btn-primary btn-sm management" href="{{ route('shop.create') }}">Créer un produit</a>
        </div>
      </div>
    </div>
  </div>
@endsection

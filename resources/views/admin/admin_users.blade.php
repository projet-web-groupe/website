@extends('layouts.app')
@section('title', 'Admin utilisateurs')
@section('content')

  <div class="banner">
    <img class="d-block w-100" src="{{ asset('assets/img/banner-admin.jpg') }}" alt="First slide"/>
    <div class="title-centered">Espace admin</div>
  </div>

  <div class="container-fluid my-3 pl-5">
    <p><a href="{{ route('admin.index') }}">Panel Admin</a> / Utilisateurs</p>

    <div class="col-12">

      <table border="1">
        <tbody>
        <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>E-mail</th>
          <th>Localisation</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
        @foreach($users as $user)
          <tr>
            <td>{{ $user->lastname }}</td>
            <td>{{ $user->firstname }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->location }}</td>
            <td>{{ $user->status }}</td>
            <td style="display: flex">
              <a class="btn btn-primary management"
                 href="{{ route('admin.user', ['user' => $user->id]) }}">Modifier</a>
              <form action="{{ route('home') }}">
                @method('DELETE')
                <button type='submit' class="btn btn-primary management" disabled>Supprimer</button>
              </form>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>

@endsection

@extends('layouts.app')
@section('title', 'Editer utilisateur')
@section('content')
  <div class="container-fluid">

    <img class="d-block w-100" src="{{ asset('assets/img/banner-admin.jpg') }}" alt="First slide" />
    <div class="container" id="user-management">
      <div class="row">

        <div class="col-12">
          <h1>Espace admin</h1>
          <p><a href="{{ route('home') }}">Panel Admin</a> / <a href="{{ route('admin.users') }}">Utilisateurs</a> / Editer un utilisateur</p>
        </div>

        <div class="col-12">
          <form method="POST" action="{{ route('admin.user.edit', ['user' => $user->id]) }}" enctype="multipart/form-data">

            @include('layouts\message')
            @if($user)
              <div class="form-row my-3"> <!-- Row to align Lastname and Firstname -->

              @csrf

              <!-- Lastname -->
                <div class="form-group col-sm-6">
                  <label for="lastname">Nom</label>
                  <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $user->lastname }}"
                         placeholder="" />
                </div>

                <!-- Firstname -->
                <div class="form-group col-sm-6">
                  <label for="firstname">Prénom</label>
                  <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $user->firstname }}"
                         placeholder="" />
                </div>
              </div>
              <!-- Email -->
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" placeholder=""
                       disabled />
              </div>

              <!-- Location-->
              <div class="form-group">
                <label for="location">Localisation</label>
                <input type="text" class="form-control" id="location" name="location" value="{{ $user->location }}"
                       placeholder="" />
              </div>

              <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Modifier</button>
              </div>
            @endif
          </form>
        </div>

      </div>
    </div>
  </div>
@endsection

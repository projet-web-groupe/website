@extends('layouts.app')
@section('title', 'Panier')
@section('content')
  <div class="container-fluid">
    @if(Session::has('cart'))
      <div class="row">
        <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

          <div class="Basket-table">
            <table class="table">
              <thead>
              <tr>
                <th scope="col" class="border-0 bg-dark">
                  <div class="p-2 px-3 text-uppercase">Nom du produit</div>
                </th>
                <th scope="col" class="border-0 bg-dark">
                  <div class="py-2 text-uppercase">Prix</div>
                </th>
                <th scope="col" class="border-0 bg-dark">
                  <div class="py-2 text-uppercase">Quantité</div>
                </th>
                <th scope="col" class="border-0 bg-dark">
                  <div class="py-2 text-uppercase">Actions</div>
                </th>
              </tr>
              </thead>
              <tbody>
              @foreach($products as $product)
                <tr>
                  <th scope="row" class="border-0">
                    <div class="p-2">
                      <img src="{{ app('App\Models\Product')->find($product['item']['id'])->photos[0]->url }}" alt=""
                           width="70" class="img-fluid rounded" />
                      <div class="ml-3 d-inline-block align-middle">
                        <h5 class="mb-0"><a href="#"
                                            class="text-dark d-inline-block align-middle">{{ $product['item']['title'] }}</a>
                        </h5><span
                          class="text-muted font-weight-normal font-italic d-block">{{ $product['qty'] }}</span>
                      </div>
                    </div>
                  </th>
                  <td class="border-0 align-middle"><strong>{{$product['price']}}€</strong></td>
                  <td class="border-0 align-middle"><strong>{{$product['qty'] }}</strong></td>

                  <td class="border-0 align-middle">
                    <a href="{{route('cart.reduceByOne', ['id' => $product['item']['id']])}}" class="text-dark"><i
                        class="fas fa-minus"></i></a>
                    <a href="{{route('cart.increaseByOne', ['id' => $product['item']['id']])}}" class="text-dark"><i
                        class="fas fa-plus"></i></a>
                    <a href="{{route('cart.removeItem', ['id' => $product['item']['id']])}}" class="text-dark"><i
                        class="fa fa-trash"></i></a>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>

        </div>
      </div>

      <div class="row py-5 p-4 bg-white rounded shadow-sm">
        <div class="col-lg-6">
          <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Votre Panier</div>
          <div class="p-4">
            <p class="font-italic mb-4">Des taxes, frais supplémentaires peuvent être ajoutés en fonction de ce que vous
              avez entré.</p>
            <ul class="list-unstyled mb-4">
              <li class="d-flex justify-content-between py-3 border-bottom"><strong
                  class="text-muted">TVA</strong><strong>0.00€</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Frais de
                  port</strong><strong>0.00€</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Prix
                  Total</strong>
                <h5 class="font-weight-bold">{{ $totalPrice }}€</h5>
              </li>
            </ul>

            <form method="POST" action="{{route('cart.buyCart')}}">
              @csrf
              <button type="submit" class="btn btn-dark rounded-pill py-2 btn-block">Procéder à l'achat</button>
            </form>

          </div>
        </div>
      </div>

    @else
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">
            <div class="Basket-table">
              <table class="table">
                <h1 class="text-center">Votre Panier est vide !</h1>
              </table>
            </div>
          </div>
        </div>

        <div class="row py-5 p-1 bg-white rounded shadow-sm text-center">
          <div class="col-lg-6">
            <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Votre Panier</div>
            <div class="p-4">
              <p class="font-italic mb-4">Des taxes, frais supplémentaires peuvent être ajoutés en fonction de ce que
                vous avez entré.</p>
              <ul class="list-unstyled mb-4">
                <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Frais de
                    port</strong><strong>0.00€</strong></li>
                <li class="d-flex justify-content-between py-3 border-bottom"><strong
                    class="text-muted">Taxe</strong><strong>0.00€</strong></li>
                <li class="d-flex justify-content-between py-3 border-bottom"><strong
                    class="text-muted">Total</strong>
                  <h5 class="font-weight-bold">0.00€</h5>
                </li>
              </ul>
              <a href="#" class="btn btn-dark rounded-pill py-2 btn-block">Procéder à l'achat</a>
            </div>
          </div>
        </div>
      </div>
    @endif
    @include('layouts\message')
  </div>
@endsection

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<!-- include info from head.blade.php -->
@include('includes.head')
<!--  import value title -->
<title>BDE - @yield('title')</title>
</head>

<body>
<!--  inclide the naviguation bar from navbar.blade.php -->
@include('includes.navbar')

<div id="app" class="wrapper">
  <!-- include the sidebar from  sidebar.blade.php-->
  @include('includes.sidebar')

  <div class="container-fluid main-container " id="content">
    <!-- import info from the view -->
    @yield('content')
  <!-- include footer from footer.blade.php-->
    @include('includes.footer')
  </div>

</div>

</body>
</html>

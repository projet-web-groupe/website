<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Developpement | BDE</title>
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://bootswatch.com/4/lux/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<!-- navigation bar -->

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="/">Dev-BDE Cesi</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
          aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="">Shop</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Articles</a>
      </li>
    </ul>
  </div>
</nav>


<!-- forms -->
<form method="post" action="{{ route('users.store') }}">
  <div class="form-group">
    @csrf
    <label for="title">Title</label>
    <label>
      <input type="text" class="form-control" name="title" placeholder="Title"/>
    </label>
  </div>
  <div class="form-group">
    <label for="body">Body</label>
    <label>
      <textarea class="form-control" name="body" cols="30" rows="10" placeholder="Body Text"></textarea>
    </label>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>

</html>

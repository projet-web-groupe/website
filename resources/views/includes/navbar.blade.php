<div id="navbar">
  <!--  bootstrap navbar -->
  <nav class="navbar navbar-expand-md bg-dark colordark fixed-top">
    <div class="container-fluid ">
      <button class="navbar-toggler bg-black m-2 ml-auto" type="button" data-toggle="collapse"
              data-target="#navbarColor02"
              aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="dark-blue-text"><i class="fas fa-bars fa-1x"></i></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarColor02">
        <ul class="navbar-nav ml-auto">
          <!-- Different link move in the website -->
          <li><a href="{{route('cart.show')}}" class="nav-link white-icon float-right">
              <i class="fas fa-shopping-basket fa-lg mr-3" aria-hidden="true"></i></a><span class="badge badge-pill badge-light">{{ Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span></li>
          <li>
            <a href="{{ route('home') }}">
              <img class="float-right d-print-block dim-little d-block d-sm-none mr-2"
                   src="https://bdecesilyon.fr/static/img/logo.png" alt=""/>
            </a>
          </li>
          <li class="nav-item active text-light text-right d-block d-md-none">
            <a href="{{ route('home') }}" class="nav-link text-uppercase">Manifestations</a>
          </li>
          <li class="nav-item active text-light text-right d-block d-md-none">
            <a href="{{ route('shop') }}" class="nav-link text-uppercase">Boutique</a>
          </li>
          <!-- Determine the user is authenticated or is a guest -->
          @guest
            <li class="nav-item active text-light text-right">
              <a href="{{ route('register') }}" class="nav-link text-uppercase">Inscription</a>
            </li>
            <li class="nav-item active text-light text-right">
              <a href="{{ route('login') }}" class="nav-link text-uppercase ">Connexion</a>
            </li>
          @else
            <li>
              <a href="{{ route('home') }}" class="nav-link white-icon float-right"></a>
            </li>
            <!-- Drop down list -->
          <li class="nav-item active dropdown text-light text-right">
            <a href="" class="nav-link dropdown-toggle text-uppercase" id="navbarDropdown" role="button" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">Mon compte</a>
            <div class="dropdown-menu colordark"  aria-labelledby="navbarDropdown">
              <a class="dropdown-item text-white" href="{{ route('account.infos') }}">Mes informations</a>
              <a class="dropdown-item text-white" href="{{  route('activities.account') }}">Mes activitées</a>
              <a class="dropdown-item text-white" href="{{ route('account.orders') }}">Mes commandes</a>
            </div>
          </li>
          <li class="nav-item active text-light text-right">
            <a href="{{ route('logout') }}" class="nav-link text-uppercase ">Deconnexion</a>
          </li>
          @endguest

        </ul>
      </div>
    </div>
  </nav>
</div>
<nav class="navbar navbar-expand-md navbar-light bg-white float-right">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ url('/') }}">
      {{ config('app.name', 'Laravel') }}
    </a>
    <button class="navbar-toggler bg-black m-2" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
      <span class="navbar-toggler-icon dark-blue-text"><i class="fas fa-bars fa-1x"></i></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <ul class="navbar-nav mr-auto">

      </ul>


      <ul class="navbar-nav ml-auto">

        <li class="nav-item">
          <a class="nav-link" href="{{ route('cart.show') }}">Panier<span
              class="badge">{{Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{ route('home') }}">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('shop') }}">Boutique</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('activities') }}">Activités</a>
        </li>
        @guest
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">Connexion</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">Inscription </a>
          </li>
        @else
          <li class="nav-item active dropdown text-light text-right">
            <a href="#" class="nav-link dropdown-toggle text-uppercase" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mon compte</a>
            <div class="dropdown-menu colordark" aria-labelledby="navbarDropdown">
              <a class="dropdown-item text-white" href="{{ route('account.infos') }}">Mes informations</a>
              <a class="dropdown-item text-white" href="#">Mes activitées</a>
              <a class="dropdown-item text-white" href="#">Mes commandes</a>
            </div>
          </li>
          <li class="nav-item active text-light text-right">
            <a href="{{ route('logout') }}" class="nav-link text-uppercase ">Deconnexion</a>
          </li>
        @endguest
      </ul>
    </div>
  </div>
</nav>

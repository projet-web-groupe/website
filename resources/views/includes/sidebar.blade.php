<nav class="d-none d-md-block fixed-top" id="sidebar">
  <div class="sidebar-header">
    <!-- Different link move in the website -->
    <a href="{{ route('home') }}"><img class="dim" src="https://bdecesilyon.fr/static/img/logo.png"
                                       alt="" /></a><br />

  </div>

  <ul class="list-unstyled components">
    <li>
      <a href="{{ route('activities') }}" class="border-bottom">Manifestations </a><br />
    </li>
    <li>
      <a href="{{ route('shop') }}" class="border-bottom">Boutique</a><br />
    </li>
    @auth
      @if(Auth::user()->isAdmin())
        <li>
          <a href="{{ route('admin.index') }}" class="border-bottom">Admin</a>
        </li>
      @endif
    @endauth
  </ul>

  <div class="container">
    <div class="row">
      <div class="pt-5 mt-5 col-12 text-center">
        <!-- Different link to social media -->
        <p>Retrouvez-nous sur</p>
        <a href="https://facebook.com" target="_blank" class="social-icons"><i class="fab fa-facebook fa-2x"
                                                                               aria-hidden="true"></i></a>
        <a href="https://twitter.com" target="_blank" class="social-icons"><i class="fab fa-twitter fa-2x"
                                                                              aria-hidden="true"></i></a>
        <a href="https://instagram.com" target="_blank" class="social-icons"><i class="fab fa-instagram fa-2x"
                                                                                aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
</nav>

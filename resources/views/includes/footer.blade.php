<footer>
  <div class="container">
    <div class="row justify-content-center sticky-bottom">
      <div class="col-12 col-sm-3 text-center ">
        <ul>
          <!-- Different link to social media/CGU/CGV/Credit -->
          <li><a href="https://www.facebook.com/BDECesiLyon/" class="containers" target="_blank"> Facebook </a></li>
          <li><a href="https://bdecesilyon.fr/media/documents/STATUTS_DU_BDE_DU_CENTRE_CESI.pdf" target="_blank"> Status
              du BDE</a></li>
          <li><a href="https://www.instagram.com/bdscesi/?hl=fr" class="containers" target="_blank"> BDS CESI Lyon </a>
          </li>
        </ul>
      </div>
      <div class=" col-12 col-sm-1 text-center">
        <ul>
          <li><a href="CGU.pdf" class="containers" target="_blank"> CGU </a></li>
          <li><img class="icon-website" src="https://bdecesilyon.fr/static/img/logo.png" alt=""/></li>
          <li><a href="CGV.pdf" class="containers" target="_blank"> CGV </a></li>
        </ul>
      </div>
      <div class=" col-12 col-sm-3 text-center">
        <ul>
          <li><a href="Legal-notice.pdf" class="legalNotice" target="_blank">Mentions légales </a></li>
          <li><a href="credits.pdf" class="credit" target="_blank">Crédits</a></li>
          <li><a href="politique-de-confidentialité.pdf" class="privacy policy" target="_blank">Politique de onfidentialité</a></li>
        </ul>
      </div>
    </div>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.1/cookieconsent.min.js"
          data-cfasync="false"></script>
  <script>
      window.cookieconsent.initialise({
          palette: {
              popup: {background: "#ff6955"},
              button: {background: "#f7fffc"},
          },
          revokable: true,
          onStatusChange: function (status) {
              console.log(this.hasConsented() ?
                  'enable cookies' : 'disable cookies');
          },
          law: {
              regionalLaw: false,
          },
          location: true,
      });
  </script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.1/cookieconsent.min.js"
          data-cfasync="false"></script>
  <script>
      window.cookieconsent.initialise({
          container: document.getElementById("content"),
          palette: {
              popup: {background: "#ff6955"},
              button: {background: "#f7fffc"},
          },
          revokable: true,
          onStatusChange: function (status) {
              console.log(this.hasConsented() ?
                  'enable cookies' : 'disable cookies');
          },
          law: {
              regionalLaw: false,
          },
          location: true,
      });
  </script>

</footer>

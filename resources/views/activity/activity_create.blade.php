@extends('layouts.app')<!-- Extend app.blade.php-->
@section('title', 'Créer une manifestation')
@section('content') <!-- Open section-->

<div class="container mt-4">
  <form method="POST" action="{{route('activities.store')}}" enctype="multipart/form-data">
    <h1 class="text-center">Créer une manifestation</h1><!-- Title of the view -->

  @include('layouts\message')

  <!-- Name of the activity -->
    <div class="form-group">
      @csrf
      <label for="title">Nom de la manifestation</label>
      <label>
          <input type="text" class="form-control" name="title" placeholder="Nom du produit" />
        </label>
    </div>

    <!-- Description of the activity -->
    <div class="form-group">
      <label for="description">Description</label>
      <label for="summernote"></label>
        <textarea class="form-control" name="description" cols="30" rows="10"
                placeholder="Description de l'objet"></textarea>
    </div>

    <div class="form-group">
      <label for="rendezvous">Date de l'évènement</label>
      <input type="date" name="rendezvous">
    </div>

    <!-- Add picture of the activity -->
    <div class="form-group">
      <label for="file">Select image to upload:</label>
      <input type="file" name="file">
    </div>

    <!-- Button to confirm the creation -->
    <button type="submit" class="btn btn-primary my-3">Générer la manifestation</button>
  </form>
</div>
@endsection


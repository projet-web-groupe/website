@extends('layouts.app')
@section('title', 'Photos '.$activity->title)
@section('content')

  <div class="container my-4">
    @include('layouts\message')

    @if(count($photos) > 0)
<h3>Voici les photos de l'activité <u>{{$activity->title}}</u></h3>
      <div class="row">
        @foreach($photos as $photo)
          <div class="col-sm-4">
            <div class="card bg-secondary mb-3" style="max-height: 30rem;">
              <img src="{{ $photo->url }}" class="img-fluid" alt="">
            </div>
          </div>

        @endforeach
      </div>

    @else
      <p>Pas de photo de cet évenement, soyez le premier a en poster! !</p>
    @endif
  </div>

@endsection

@extends('layouts.app')
@section('title', 'Manifestations')
@section('content')

  <div class="container-fluid mt-4" id="activity">
    @include('layouts\message')

    @if(count($activities) >= 3)
      <h3 class="title">Manifestations à venir :</h3>
      <div class="col-lg">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">
            <li data-target="#carouselExampleIndicators" data-slide-to="1">
            <li data-target="#carouselExampleIndicators" data-slide-to="2">
          </ol>

          <div class="carousel-inner" style="max-height: 40vh">
            @for ($i = 0; $i < 3; $i++)
              @if($i == 0)
                <div class="carousel-item active">
                  <img class="d-block w-100" src="{{$activities[$i]->photos[0]->url}}" alt="Second slide"/>
                </div>
              @else
                <div class="carousel-item">
                  <img class="d-block w-100" src="{{$activities[$i]->photos[0]->url}}" alt="Second slide"/>
                </div>
              @endif
            @endfor

          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    @endif

    @if(count($activities) > 0)
      <div class="container-fluid">
        <div class="row">
          <h3 class="title"><br/>Les manifestations : </h3>
        </div>
        <div class="row">
          @foreach($activities as $activity)
            <div class="col-12 col-sm-4 mb-3">
              <div class="card">

                @if(count(($activity->photos)) > 0)
                  <img src="{{$activity->photos[0]->url}}" class="img-fluid" alt="image de la manifestation">
                @else
                  <img class="img-fluid" src="" alt="">
                @endif
                <div class="card-body">
                  <div class="card-header">{{$activity->title}}</div>
                  <p class="card-title"><strong>{{$activity->firstname}}</strong></p>
                  <p class="card-text">{!! $activity->description !!}</p>

                  <a href="/manifestations/{{$activity->id}}"
                     class="col text-center space btn btn-primary">Détails</a>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>

    @else
      <p class="text-center font-weight-bold">Pas de d'activité !</p>
    @endif
  </div>

@endsection

@extends('layouts.app')<!-- Extend app.blade.php-->
@section('title', 'Editer une manifestation')
@section('content') <!-- Open section-->

<div class="container my-4">
  @include('layouts\message')
  <form method="POST" action="{{route('activities.update', $activity->id)}}" enctype="multipart/form-data">
    <h1>Editer la manifestation</h1>
    <div class="form-group">
      @method('PUT')
      @csrf
      <label for="title">Nom du activité</label>
      <input value="{{ $activity->title }}" type="text" class="form-control" name="title"
             placeholder="Nom du activité" />
    </div>

    <div class="form-group">
      <label for="description">Description</label>
      <textarea class="form-control" name="description" cols="30" rows="10"
                placeholder="Description de le activité">{{ $activity->description }}</textarea>
    </div>

    <div class="form-group">
      <label for="rendezvous">Date de l'évènement</label>
      <input type="date" name="rendezvous">
    </div>

    <div class="form-group">
      <label for="file">Selectionner image qui remplacera:</label>
      <input type="file" name="file">
    </div>

    <button type="submit" class="btn btn-primary">Modifier</button>
  </form>
</div>
@endsection

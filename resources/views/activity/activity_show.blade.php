@extends('layouts.app')
@section('title', $activity->title)
@section('content')

  <div class="container-fluid my-4">
    @include('layouts\message')
    <div class="" id="single-activity">
      <div class="row justify-content-center">
        <div class="col-sm my-3">
          @if(count(($activity->photos)) > 0)
            <img src="{{$activity->photos[0]->url}}" class="img-fluid" alt="">
          @else
            <img class="img-fluid" src="" alt="">
          @endif
        </div>

        <div class="col-md-6 col-12 text-center account-summary">
          <h1 class="display-3">{{$activity->title}}</h1>
          <p class="lead my-2">{!!$activity->description!!}</p>
          <p class="lead my-1"><time datetime="{{$activity->rendezvous}}">Date de l'évènement: {{date_format(date_create($activity->rendezvous),"d/m/Y")}}</time></p>
          <hr class="my-4">
          <p class="lead">

          @auth
            @if(!in_array(Auth::user()->id, $likesId))
              <form method="POST" action="{{route('activities.postLike', $activity->id)}}">
                <h1>Lache un like !</h1>
                @csrf
                <button type="submit" class="btn btn-success">LIKE</button>
              </form>
            @else
              <form method="POST" action="{{route('activities.removeLike', $activity->id)}}">
                <h1>Lache un like !</h1>
                @csrf
                <button type="submit" class="btn btn-danger">UNLIKE</button>
              </form>
            @endif
          @endauth

          <p>{{count($activity->likes)}} j'aimes</p>

          @auth
            @if(!in_array(Auth::user()->id, $usersId))
              <form class="float-left" method="POST" action="{{route('activities.join', $activity->id)}}">
                @csrf
                <button type="submit" class="btn btn-primary">Rejoindre la manifestation</button>
              </form>
            @else
              <form class="float-right" method="POST" action="{{route('activities.leave', $activity->id)}}">
                @csrf
                <button type="submit" class="btn btn-outline-danger">Quitter la manifestation</button>
              </form>
            @endif

            @if(Auth::user()->isAdmin())
              <a class="btn btn-dark btn-lg" href="{{ route('activities.edit', $activity->id) }}" role="button">Editer</a>

              <form class="float-left" method="POST" action="{{ route('activities.destroy', $activity->id) }}">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-danger">Supprimer la manifestation</button>
              </form>
            @endif
          @endauth
        </div>
      </div>

      <div class="container">
        <h3>Commentaires</h3>
        @if (count($comments) > 0)
          @foreach($comments as $comment)
            <div style="background-color: rgba(169,158,169,0.62)">
              <blockquote class="blockquote">
                <p class="mb-0">{!! $comment->comment !!}</p>
                <footer class="blockquote-footer">Posté à {{ $comment->updated_at }} par <cite
                    title="Source Title">{{ $comment->user->firstname }}</cite>
                </footer>
              </blockquote>
            </div>
          @endforeach
        @else
          <p>Il n'y a pas de commentaires</p>
        @endif
      </div>

      <form method="POST" action="{{route('activities.postComment', $activity->id)}}">
        <h1>Poster un commentaire</h1>
        <div class="form-group">
          @csrf
          <div class="form-group">
            <label for="summernote"></label>
            <textarea class="form-control" name="comment" cols="30" rows="10" placeholder="Poster un comment"></textarea>
          </div>
          <button type="submit" class="btn btn-primary">Valider</button>
        </div>
      </form>

      <form method="POST" action="{{route('activities.postPhoto', $activity->id)}}" enctype="multipart/form-data">
        <h1>Poster une photo de l'activité</h1>
        <h5>Attention veuillez respecter la charte du BDE</h5>
        <div class="form-group">
          @csrf
          <label for="file">Select image to upload:</label>
          <input type="file" name="file">
        </div>
        <button type="submit" class="btn btn-primary">Valider</button>
      </form>
      <a type="button" href="{{route('activities.showAlbum', $activity->id)}}" class="btn btn-primary btn-sm">Voir les
        photos de l'activité</a>
    </div>
  </div>
@endsection
